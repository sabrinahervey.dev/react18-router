import "./Navigation.css";
//import des liens de react-router
import { Link } from 'react-router-dom';


//Fonction qui represente la barre de navigation
function Navigation() {
  return (
    <ul className="Navigation">
      {/* Liens qui redirige vers les pages lorsque on clique dessus */}
      <li><Link to="/">Home</Link></li>
      <li><Link to="/Products">Products</Link></li>
      <li><Link to="/Cart">Cart</Link></li>
    </ul>
  );
}

export default Navigation;
