import React from "react";
import ReactDOM from "react-dom/client";
import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";
import "./index.css";

// import des éléments depuis react-router-dom
import {Link, NavLink, RouterProvider, createBrowserRouter} from "react-router-dom";

//creation des differentes routes et de leur elements associés
const router = createBrowserRouter([
  {
    path: "/",
    //l'élément associé au chemin "/" est le composant Home
    element: <Home></Home>
  },
  {
    path: "/Products",
    //l'élémént associé au chemin "Products" est le composant productlist
    element: <ProductList></ProductList>
  },
  {
    path: "/Cart",
    //l'élément associé au chemin "Cart" est le composant Cart
    element: <Cart></Cart>
  }
])
 

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* RouterProvider fournit les informations à l'application */}
    <RouterProvider router={router} />
  </React.StrictMode>
);
